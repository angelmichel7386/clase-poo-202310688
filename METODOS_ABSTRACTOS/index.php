<?php 
abstract class Transporte {

    abstract  public function  mantenimiento();

}

class Avion extends Transporte{ 

    public function mantenimiento(){
        echo " revision de alas ";
    }
   

}

class Automovil extends Transporte{

    public function mantenimiento(){
        echo " cambio  de   aceite ";
    }
    
}
class bicicleta  extends Transporte{

    public function mantenimiento(){
        echo " revision de frenos ";
    }


}

class tractor  extends Transporte{

    public function mantenimiento(){
        echo " controlar baterias  ";
    }


}

class motocicleta  extends Transporte{

    public function mantenimiento(){
        echo " revision de llantas   ";
    }


}


$obj = new Avion ();
$obj->mantenimiento();

$obj2 =new Automovil();
$obj2->mantenimiento();

$obj3 = new bicicleta ();
$obj3->mantenimiento();

$obj4 = new tractor ();
$obj4->mantenimiento();

$obj5 = new motocicleta();
$obj5->mantenimiento();
?>
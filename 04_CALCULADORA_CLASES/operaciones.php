<?php

//clase operaciones
class operaciones{

    //atributo operacion a realizar
    private $operacionrealizar = "";

    public function __construct($operacionrealizar){
        $this->operacionrealizar=$operacionrealizar;
    }


    //metodo de suma con parametro a y b
    public function Suma($a,$b){
        return $a + $b;
   }
    //metodo de resta con parametro a y b
    public function Resta($a,$b){
        return $a-$b;
   }
    //metodo de division con parametro a y b
    public function Division($a,$b){
        return $a/$b;
   }
    //metodo de multiplicacion con parametro a y b
    public function Multiplicacion($a,$b){
        return $a*$b;
   }
    //metodo que realiza las operaciones de metodos en base a la propiedad $operacionrealizar
    public function resultadoOperacion($a,$b){

    switch ($this->operacionrealizar ) {
        case 'suma':
            return $this->Suma($a,$b);
            break;
        case 'resta':
            return $this->Resta($a,$b);
            break;
         case 'division':
            return $this->Division($a,$b);
            break;
         case 'multiplicacion':
            return $this->Multiplicacion($a,$b);
            break;
        default:
            return 'operacion a realizar no definida';
            break;
    }

   }


}






?>